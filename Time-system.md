# Time system

## Hours :
Days are made of 24 Moons in Hereva, similar to our 24 hours. 12 hour clock format with 'PM' or 'AM' uses PinkMoon and AirMoon. 24 hour clock format uses Moons. _(eg. 3PM = 3 PinkMoon = 15 Moons, same for 2AM = 2 AirMoon = 2 Moons)_

* **PinkMoon** : Similar to PM (post meridiem, "after midday")


* **AirMoon** :
Similar to AM (from the Latin ante meridiem, meaning "before midday")


## The Herevan Calendar

Hereva's calendar is based on a 13 month calendar consisting of four weeks with seven days per week. The months are rather dull in Hereva, named First, Second, Third, and so on. At one time there were more creative names for the months, but were changed after an egregious error in tax collections occurred. The error happened when someone preparing the forms misheard "Thistlebury" as "Sisalbury" as the due date. This caused no end of confusion for everyone involved, and the tax collection for that year happened much later than anticipated. From then on the months have been ordered by the Royal Collection Agency to be brief and unambiguously named. Such are the ways of Royal Bureaucracy.

Each of the days has a name relating to each of the six schools of magic in Hereva, with the seventh day denoted as a day for all of the schools (or, as some cynics might infer, none of the schools).  The names were chosen to follow an alphabetic sequence: A, B, C, D, E, and F, with the seventh day breaking convention by using "0x".

The days are:

* **Azarday**: Coming from the French word 'Hazard' wrote in phonetic 'Azar'. It's the day of 'Chaosah'. In Hereva culture, it's similar Sunday. A free day of work. Events, Challenges often happen on Azarday._(ref: episode 3, Potion challenge on Azarday)_ , _Character in relation : Pepper_

* **Babkaday**: Coming from the eastern of Europe ( Polish, Belarusian, Ukrainian and Russian ) describing a brioche like sweet yeast cake. Babkaday is the day of 'Magmah'. _Character in relation : Saffron_

* **Cetoday**: Coming from the 'Ceto', the ancient Greek primordial sea goddess or sea monster. This is the day of 'Aquah'.

* **Donday**: Coming from Irish and Celtic mythology god:  Donn, or the Dark One, is the Lord of the Dead. This day is linked to the magic of death, 'Zombiah'. _Character in relation : Coriander_

* **Eggday**: Coming from the English word 'egg', this is the well known symbol for many representation of life. This is the day in relation to 'Hippiah'.

* **Fookuday**: Coming from the god Fukurokuju ( fuku meaning "happiness" ). He is one of the Seven Lucky Gods in Japanese mythology. This day is in relation to the magic of 'Ah'. _Character in relation : Shichimi_

* **Zero's Day**: Coming from the number zero, which represents all of the schools of Hereva, this day is considered a special day of rest and relaxation.

  - Most of Hereva spends this day relaxing, though many spider admins tend to work on this day because the increased traffic from browsing tends to stress the network. The spider admins have a saying that they get no rest on Zero's Day after putting in long hours ensuring the network is operational.
  - There is a common misconception that Zero's day was named after the famous spider admin / philosopher Zero, who devised Zero's Paradox. The paradox began as a question: if a packet traverses half of a network cable, and again traverses half the remaining network cable, will the packet ever reach its destination? While this does have deep philosophical implications for our concept of infinity it was less well-received by Zero's management, who found it's use as an explanation for why the network was running so slow rather unsatisfying. 
  - The notation "0x" is used as an abbreviation for Zero's day as it is a useful way to denote that the day celebrates all of the magical schools and doesn't single out any particular one for that day. It also celebrates the non-magical creatures of Hereva, and signifies new beginnings. _(ref: episode 23, the Genie of Success refers to all seven weekdays as times he enables success)_
  - The proper way to denote all seven days of Hereva's week is to use the following notation: "0xABCDEF". This convention came about because ABCDEF0x, according to the Hereva Institute of Standards and Measures, "looked really off, and should never have been considered in the first place".
