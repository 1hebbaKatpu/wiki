# Creatures

## Creatures of Hereva 

Hereva has many different creatures roaming through the various lands. Dragons are the oldest of these creatures and predate the known history of Hereva. Many various creatures roam Hereva, and many more are being discovered after the Great Tree of Komona's explosion ripped the magical fabric of Hereva apart. This has kept a number of scientists busy trying to classify the previously unknown species. To date there are many strange combinations of creatures ranging from the fearsome Phanda to the peaceful Dragon-Cow. Expeditions into Hereva's forests and uncharted lands are known to bring new discoveries without fail. Some scientific communities have derided these as "phishing and pherreting expeditions", so named for the famous scientist Dr. Pomme who uncovered a pheasant / fish and a pheasant / ferret combination in the same day. "Dr. Pomme just picks the low-hanging fruit to document" they claim. Dr. Pomme dismisses these assertions as petty, claiming that even the modest discoveries add value to the scientific literature. Dr. Pomme has planned many daily expeditions to find new creatures. His graduate students just call these expeditions "Pommes". The graduate students find they can get more work done while he's out of the office, even though his expeditions bring more discoveries that they have to catalog and manage. They don't mind, though, because his micro-managing makes their jobs more difficult. The relieved grad students have a saying: "A Pomme a day keeps the doctor away". 

Listed below are a sampling of Dr. Pomme's discoveries, one from each volume of his 80 volume (and counting) work. These are dedicated to the tireless graduate students who have carefully documented, cataloged, and typeset all 80 (sorry, 81) volumes of this work. It is truly a testament to Dr. Pomme's research and their efforts that these 82 volumes are not only useful to the scientific community, but also a pleasure to read. We can't thank them enough for the quality of this 83 volume text.

## Bestiary

### Argiope Spiders

![Argiope](https://www.peppercarrot.com/data/wiki/medias/img/creature_argiope.jpg)
* **Type**: Arachnid
* **Place**: Usually around the red berry trees in The Haunted Jungle.
* **Episodes**: ep03
* **Description**: Argiope spiders are yellow spiders that are primarily found in The Haunted Jungle. Their most distinguishing feature is a grayish spiral on their body, either on the thorax or the abdomen. They are one of the few spiders in Hereva that hunt in packs, using an intricate system of webbing, signals, and gestures for capturing prey. They also have a remarkably well-developed hierarchy that is still being studied by Hereva's scientists. What can seem like a small batch of curious Argiope spiders might actually be scouting parties keeping watch over their territory and notifying the others as needed. There are stories of Argiope scouts drawing large numbers of Argiope spiders to take down their prey in what one researcher described as "methodical". The red berry trees have proven the most fruitful places for Argiope spiders to congregate because many of their prey cannot resist the taste of the red berries. 

### Phoenix

![Phoenix](https://www.peppercarrot.com/data/wiki/medias/img/creature_phoenix.jpg)
* **Type**: Magical birds.
* **Place**: The Valley of Volcanoes
* **Episode**: ep03
* **Description**: Phoenix are large birds with long feathers on the top of their heads. They are made of magma and fire, and shine with a fiery brilliance. They are near-immortal with their regenerative powers. At the end of a phoenix's life they return to the Valley of Volcanoes. When their bodies are no longer able to contain the magma and fire that binds them together they are consumed by the magma and fire and turn into ash. Those ashes sprinkle down into the magma of the volcano, where it reforms the phoenix. Phoenixes in captivity do not thrive because they are unable to regenerate (some folks have tried to capture the phoenix but are left with nothing but inert ashes and charred cages when the phoenix dies). Female phoenixes can have around three eggs per year. It is difficult for the untrained eye to discern between the male and the female phoenix, and usually can only be done at temperatures that are uncomfortable for most beings.

### DragonCow

![DragonCow](https://www.peppercarrot.com/data/wiki/medias/img/creature_dragoncow.jpg)
* **Type**: Dragon, female.
* **Place**: Sky over Squirrel's End forest
* **Description**: Very large dragon looking like a cow. It spends most of the day chewing trees and calmly gliding over Hereva.
* **Episode**: ep03

### Fisher Owl

![Fisher Owl](https://www.peppercarrot.com/data/wiki/medias/img/creature_fisher-owl.jpg)
* **Type**: Owl, female.
* **Place**: Squirrel's End forest
* **Description**: A lazy owl able to practice fishing to hunt mice with a piece of cheese.
* **Episode**: ep04

### Savant-Ant

![Savant-Ant](https://www.peppercarrot.com/data/wiki/medias/img/creature_savant-ant.jpg)
* **Type**: ants, neutral.
* **Place**: Squirrel's End, around the house of Pepper
* **Description**: A group of genius ants, affected by Pepper's Potion of Genius that she assumed had failed (ep4). Their progression drew Pepper's attention when they launched their first space-borne mission in her garden (ep19).
* **Episode**: ep04, ep19

### Dragon-Drake

![Dragon-Drake](https://www.peppercarrot.com/data/wiki/medias/img/creature_dragon-drake.jpg)
* **Type**: Dragon, male.
* **Place**: Around Komona city, in the sky
* **Description**: A small dragon duck/drake looking for love.
* **Episode**: ep06

### Oversized-Posh-Zombie-Canary

![Oversized-Posh-Zombie-Canary](https://www.peppercarrot.com/data/wiki/medias/img/creature_oversized-posh-zombie-canary.jpg)
* **Type**: Canary, female.
* **Description**: A big monster, result of too many potions on a dead canary after the potion challenge.
* **Episode**: ep06

### Old-Sylvan

![Old-Sylvan](https://www.peppercarrot.com/data/wiki/medias/img/creature_old-sylvan.jpg)
* **Type**: Magical tree, male.
* **Place**: Squirrel's End.
* **Description**: Being a part of Pepper's security system, Old-Sylvan captures intruders with his lianas. A little squirrel lives in his mouth.
* **Episode**: ep09

### Theorem-the-Golem

![Theorem-the-Golem](https://www.peppercarrot.com/data/wiki/medias/img/creature_theorem-the-golem.jpg)
* **Type**: Golem, male.
* **Place**: Moutain behind Squirrel's End.
* **Description**: Part of Pepper's security system, he lives with pigeons.
* **Episode**: ep09

### Dragonmoose

![Dragonmoose](https://www.peppercarrot.com/data/wiki/medias/img/creature_dragonmoose.jpg)
* **Type**: Dragon, male.
* **Place**: Clouds, on the top of moutains behind Squirrel's End.
* **Description**: A big, hairy dragon with a friendly moose head. Females can have three colored eggs (from pastel blue to yellow).
* **Episode**: ep09

### Bigfish

![Bigfish](https://www.peppercarrot.com/data/wiki/medias/img/creature_bigfish.jpg)
* **Type**: Fish, male.
* **Place**: Sea side.
* **Description**: A very big fish which can eat small islands and all kinds of sea creatures.
* **Episode**: ep10

### Spider-Admin

![Spider-Admin](https://www.peppercarrot.com/data/wiki/medias/img/creature_spider-admin.jpg)
* **Type**: Spiders.
* **Place**: In caverns located in forests of Hereva.
* **Description**: Big spiders which manage a crystal ball network.
* **Episode**: ep15

### Phanda

![Phanda](https://www.peppercarrot.com/data/wiki/medias/img/creature_phanda.jpg)
* **Type**: Combination of Elephant and Badger
* **Place**: Forests of Hereva (exact location unknown)
* **Description**: The Phanda is a large, furry creature with the face and trunk of an elephant and the body of a badger. It has horns along its back and is considered quite dangerous, especially to unwary travelers in the jungles and forests of Hereva.
* **Episode**: ep17

### Genie of success

![Genie of success](https://www.peppercarrot.com/data/wiki/medias/img/creature_genie_of_success.png)
* **Type**: Genie with a walrus head
* **Place**: In a small magic lamp
* **Description**: Genie of success when appearing can grant you the doorway to success after signing a contract
* **Episode**: ep23

### Scorpio

* **Type**: Scorpion like creature
* **Place**: Castle in episode 38
* **Description**: Is part of the monsters defending the treasure.
* **Episode**: ep38

### Shapeshifter

* **Type**: Hind-legged monster with long fangs and nails
* **Place**: Castle in episode 38
* **Description**: Is part of the monsters defending the treasure. It can evolve to look even more ferocious with spikes on its back. During evolution it emits a high-pitched scream.
* **Episode**: ep38

## Demons of Chaosah

### Hornük

![Hornük](https://www.peppercarrot.com/data/wiki/medias/img/creature_hornuk.jpg)
* **Type**: Demon of Chaosah, male.
* **Place**: Dimension Chaosah, can be summoned for a short time to Hereva's main reality.
* **Description**: A mid-sized, green-haired demon with horns and claws. He likes cupcakes.
* **Episode**: ep08

### Eyeük

![Eyeük](https://www.peppercarrot.com/data/wiki/medias/img/creature_eyeuk.jpg)
* **Type**: Demon of Chaosah, male.
* **Place**: Dimension Chaosah, can be summoned for a short time to Hereva's main reality.
* **Description**: A mid-sized, red-haired demon with four eyes and big muscles. He leads the mid-sized demons and has a trident weapon in hand.
* **Episode**: ep08

### Spidük

![Spidük](https://www.peppercarrot.com/data/wiki/medias/img/creature_spiduk.jpg)
* **Type**: Demon of Chaosah, female.
* **Place**: Dimension Chaosah, can be summoned for a short time to Hereva's main reality.
* **Description**: A mid-sized, purple spider demon. She likes tea.
* **Episode**: ep08

## Dragons of Hereva

Dragons roamed the lands of Hereva for as long as any memory can recall. The time they aren't soaring over Hereva's landscape they spend guarding their lairs. They are capricious beings, with their own desires and agendas. There is even a myth that the sun of Hereva is comprised of two dragons fighting a continual battle and launching great bursts of flame at each other. Most dragons do not elaborate on this myth, and will quickly change the subject should it come up.

They regard humans as curiosities and are fascinated that beings with such short lifespans are constantly inquiring about events they can't possibly know.

There is a type of dragon known as the Petite Dragon that has been known to be around members of Ah. Whether these can be considered "true" dragons or another species on Hereva has yet to be determined. After a number of years the members of Ah release these Petite Dragons into the wild, never to see them again. It is difficult to keep track of dragons in the wild as magical tagging never seems to stick on them for long. Some believe the dragons seen with members of Ah are actually young dragons, and the reason they often return to the land of the setting moons is because that is their home. Whatever the case, the dragons and members of Ah will not elaborate on their relationship.

Near the end of a dragon's life, it will head toward the three setting moons, near the temples of Ah. There it will spend the rest of it's days in quiet rest and contemplation. The members of Ah are respectful and do not disturb any of the dragons in the area unless the dragon initiates conversation. To disturb a dragon in these final days is considered anathema, and carries stiff punishments.

## Dragons and Hereva's Pre-History

The Dragons of Hereva have lived in the land of Hereva long before human recollection. It wasn't until The Great Tree of Komona that the dragons took notice of the humans in Hereva. Whether that was because the humans didn't arrive until The Great Tree of Komona appeared is something the Komonan scholars wrestle to resolve. Many conversations with the dragons prove inconsistent at best and obfuscated at worst. One theory is that the dragons simply did not pay any attention to the humans until they wielded magic, much in the same way that humans don't pay attention to the ants until they start building superstructures in their back yard. Another theory is the dragons purposefully obfuscate human history, answering in riddles and conflicting details in the hopes of keeping humans away from some hidden lore.

### Red Dragon (Fire Dragon)

![Red Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_red-dragon.jpg)
* **Type**: Dragon
* **Place**: Caves
* **Description**: A scaly, horned dragon with three horns on either side of its head and two horns on top. They have a crest near the back of their head with additional horns protruding. They are winged creatures and protect their lairs with fiery breath.
* **Episode**: ep14

### Air Dragon

![Air Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_air-dragon.jpg)
* **Type**: Dragon
* **Place**: Flying above Hereva
* **Description**: Air Dragons have webbing on the sides of their long, aerodynamic heads. They are green in color, and are seen briefly in episode 14.
* **Episode**: ep14

### Swamp Dragon

![Swamp Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_swamp-dragon.jpg)
* **Type**: Dragon
* **Place**: In the Swamps of Hereva
* **Description**: Swamp Dragons are muddy creatures in the swamps of Hereva. They are normally docile creatures, but can project a forceful spray of mud at oncoming attackers.
* **Episode**: ep14

### Lightning Dragon

![Lightning Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_lightning-dragon.jpg)
* **Type**: Dragon
* **Place**: In the Mountains of Hereva
* **Description**: Lightning Dragons are elemental creatures made of pure energy. They are usually seen in their fearsome dragon form. They tend to be more curious than aggressive and take great pleasure in their occasional visitors.
* **Episode**: ep14
